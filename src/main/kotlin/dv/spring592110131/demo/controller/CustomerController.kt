package dv.spring592110131.demo.controller

import dv.spring592110131.demo.entity.Customer
import dv.spring592110131.demo.service.CustomerService
import dv.spring592110131.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.persistence.Id

@RestController
class CustomerController {
    @Autowired
    lateinit var customerService: CustomerService

    @GetMapping("/customer")
    fun getAllCustomer(): ResponseEntity<Any> {
//        val customers = customerService.getCustomer()
        return ResponseEntity.ok(customerService.getCustomer())
    }

    @GetMapping("/customer/query")
    fun getCustomers(@RequestParam("name") name: String): ResponseEntity<Any> {
        return ResponseEntity.ok(customerService.getCustomerByName(name)!!)
//        output?.let { return ResponseEntity.ok(it) }
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/customers/partialQuery")
    fun getCustomerPartial(@RequestParam("name") name: String, @RequestParam("email", required = false) email: String?)
            : ResponseEntity<Any> {
     return ResponseEntity.ok(customerService.getCustomerByPartialNameEmail(name,name))
    }

    @GetMapping("/customers/End")
    fun getCustomerEnd(@RequestParam("name") name: String
    ): ResponseEntity<Any> {
        return ResponseEntity.ok(customerService.getCustomerEnd(name))
//        return  output
//        output?.let { return ResponseEntity.ok(it) }
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
    @GetMapping("/customer/{manuName}")
    fun getCustomerByManuName(@PathVariable("manuName") name:String):ResponseEntity<Any>{
        return ResponseEntity.ok(customerService.getCustomerByProvince(name))
    }
    @GetMapping("/customers/province")
    fun getCustomerByManuNameV2(@RequestParam("province") name:String):ResponseEntity<Any>{
        return ResponseEntity.ok(customerService.getCustomerByProvince(name))
    }

    @PostMapping("customer/defaultAddress/{id}")
    fun addCustomer(@RequestBody customer : Customer,@PathVariable ("id") addId:Long):ResponseEntity<Any> {
        return ResponseEntity.ok(customerService.save(addId,customer))
    }
    @PostMapping("customer/defaultAddress")
    fun addCustomerAndaddress(@RequestBody customer : Customer):ResponseEntity<Any> {
        return ResponseEntity.ok(customerService.saveAddress(customer))
    }

//    @DeleteMapping("/customer/{id}")
//    fun deleteProduct(@PathVariable("id")id: Long):ResponseEntity<Any>{
//        return ResponseEntity.ok(customerService.)
//    }

}