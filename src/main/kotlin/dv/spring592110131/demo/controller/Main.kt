//package dv.spring592110131.demo.controller
//
//import ch.qos.logback.core.pattern.util.RegularEscapeUtil
//import dv.spring592110131.demo.entity.Product
//import dv.spring592110131.demo.entity.myPerson
//import org.springframework.http.ResponseEntity
//import org.springframework.web.bind.annotation.*
//
//@RestController
//
//class Main {
//    @GetMapping("/getAppName")
//    fun getHelloWorld(): String {
//        return "assessment"
//    }
//
//    @GetMapping("/product")
//    fun getProduct(): ResponseEntity<Any> {
//        val product = Product("iPhone", "A new telephone", 28000, 5)
//        return ResponseEntity.ok(product)
//    }
//
//    @GetMapping("/product/{name}")
//    fun getIphone(@PathVariable("name") name: String): ResponseEntity<Any>  {
//        val product = Product("iPhone", "A new telephone", 28000, 5)
//        if (product.name  ==  name ){
//            return ResponseEntity.ok(product)
//        }else {
//            return ResponseEntity.notFound().build()
//        }
//
//
//
//
//
//    }
//    @PostMapping("/setZeroQuantity")
//    fun setZeroQuantity(@RequestBody product: Product): ResponseEntity<Any> {
//        product.quantity = 0
//        return ResponseEntity.ok(product)
//
//    }
//    @PostMapping("/totalPrice")
//    fun totalPrice(@RequestBody product: Array<Product>): String {
//        var total =0
//        for (i in product ){
//            total +=i.price
//        }
//        return "$total"
//
//    }
//    @PostMapping("/avaliableProduct")
//    fun avaliableProduct(@RequestBody product: Array<Product>): ResponseEntity<Any> {
//        val list = mutableListOf<Product>()
//        for (i in product) if (i.quantity!==0){
//            list.add(i)
//        }
//        return ResponseEntity.ok(list)
//
//    }
//
//
//}