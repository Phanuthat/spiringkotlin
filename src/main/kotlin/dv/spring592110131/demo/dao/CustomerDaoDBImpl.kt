package dv.spring592110131.demo.dao

import dv.spring592110131.demo.entity.Customer
import dv.spring592110131.demo.repository.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Repository
@Profile("db")
class CustomerDaoDBImpl : CustomerDao {
    override fun save(customer: Customer): Customer {
        return customerRepository.save(customer)
    }

    override fun getCustomerByProvince(name: String): List<Customer> {
        return customerRepository.findByDefaultAddress_ProvinceContainingIgnoreCase(name)
    }

    override fun getProductByPartialNameAndEmil(name: String, email: String): List<Customer> {
        return customerRepository.findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name,email)
    }

    override fun getCustomersEnd(name: String): List<Customer> {
        return  customerRepository.findByNameEndingWith(name)
    }


    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerRepository.findByNameContaining(name)
    }

    override fun getCustomerByName(name: String): Customer? {
        return customerRepository.findByName(
                name
        )


    }

    @Autowired
    lateinit var customerRepository: CustomerRepository

    override fun getCustomers(): List<Customer> {
        return customerRepository.findAll().filterIsInstance(Customer::class.java)
    }

}