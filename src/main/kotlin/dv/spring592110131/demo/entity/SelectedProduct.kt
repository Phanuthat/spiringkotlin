package dv.spring592110131.demo.entity

import javax.persistence.*

@Entity
data class SelectedProduct(var quantity :Int){
    @Id
    @GeneratedValue
    var id:Long? = null
    @OneToOne
//    var products :Product?  =null
    lateinit var product :Product

}  