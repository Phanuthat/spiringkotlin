package dv.spring592110131.demo.entity

import javax.persistence.*

@Entity

data class ShoppingCart(var shoppingCartStatus: ShoppingCartStatus) {
    @Id
    @GeneratedValue
    var id: Long? = null
    @OneToMany
    var SelectedProduct = mutableListOf<SelectedProduct>()
    @OneToOne
    var shippingAddress: Address?=null
    @OneToOne
     var Customer: Customer?=null
}
