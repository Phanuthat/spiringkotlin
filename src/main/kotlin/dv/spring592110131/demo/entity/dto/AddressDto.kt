package dv.spring592110131.demo.entity.dto

data class AddressDto(  var homeAddress: String?=null,
                        var subdistrict : String?=null,
                        var province : String?=null,
                        var postCode:String?=null,
                        var id:Long?=null)
