package dv.spring592110131.demo.entity.dto

data class ProductDto(var name: String? = null,
                      var description: String? = null,
                      var price: Double? = null,
                      var amountInStock: Int? = null,
                      var imageUrl: String? = null,
                      var manu :ManufacturerDto?=null,
                      var id:Long?=null,
                      var isDeleted:Boolean? = null )


