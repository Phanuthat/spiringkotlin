package dv.spring592110131.demo.repository

import dv.spring592110131.demo.entity.Manufacturer
import org.springframework.data.repository.CrudRepository

interface ManufacturerRepository: CrudRepository<Manufacturer,Long>{
    fun findByName(name:String):Manufacturer
}