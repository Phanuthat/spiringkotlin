package dv.spring592110131.demo.repository

import dv.spring592110131.demo.entity.SelectedProduct
import org.springframework.data.repository.CrudRepository

interface SelectedProductRepository:CrudRepository<SelectedProduct,Long>