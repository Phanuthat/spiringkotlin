package dv.spring592110131.demo.repository

import dv.spring592110131.demo.entity.ShoppingCart
import org.springframework.data.repository.CrudRepository

interface ShoppingCartRepository:CrudRepository<ShoppingCart,Long>