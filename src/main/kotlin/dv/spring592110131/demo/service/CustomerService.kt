package dv.spring592110131.demo.service

import dv.spring592110131.demo.entity.Customer
import dv.spring592110131.demo.entity.Product

interface CustomerService {
    fun getCustomer(): List<Customer>
    fun getCustomerByName(name:String): Customer?
    fun getCustomerByPartialName(name:String):List<Customer>
    fun getCustomerEnd(name: String): List<Customer>
    fun getCustomerByPartialNameEmail(name: String, email: String): List<Customer>
    fun getCustomerByProvince(name: String): List<Customer>
    fun save(addId:Long,customer: Customer):Customer
    fun saveAddress(customer: Customer):Customer

}