package dv.spring592110131.demo.service

import dv.spring592110131.demo.dao.AddressDao
import dv.spring592110131.demo.dao.CustomerDao
import dv.spring592110131.demo.entity.Customer
import dv.spring592110131.demo.entity.Product
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class CustomerServiceImpl:CustomerService {
    @Transactional
    override fun saveAddress(customer: Customer): Customer {

        val address=customer.defaultAddress?.let {addressDao.save(it)  }
        val customer=customerdao.save(customer)

        return customer
    }

    @Autowired
    lateinit var addressDao: AddressDao
    @Transactional
    override fun save(addId: Long, customer: Customer): Customer {
       val address=addressDao.findById(addId)
        val customer =customerdao.save(customer)
        customer.defaultAddress=address

        return customer
    }

    override fun getCustomerByProvince(name: String): List<Customer> {
        return customerdao.getCustomerByProvince(name)
    }

    override fun getCustomerByPartialNameEmail(name: String, email: String): List<Customer> {
        return customerdao.getProductByPartialNameAndEmil(name, email)
    }

    override fun getCustomerEnd(name: String): List<Customer> {
        return customerdao.getCustomersEnd(name)
    }

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return  customerdao.getCustomerByPartialName(name)
    }

    override fun getCustomerByName(name: String): Customer?

        = customerdao.getCustomerByName(name)


    @Autowired
    lateinit var customerdao : CustomerDao

    override fun getCustomer(): List<Customer> {
        return customerdao.getCustomers()
    }




}