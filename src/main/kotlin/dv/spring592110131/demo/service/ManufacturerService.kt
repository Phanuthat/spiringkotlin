package dv.spring592110131.demo.service

import dv.spring592110131.demo.entity.Manufacturer
import dv.spring592110131.demo.entity.dto.ManufacturerDto

interface ManufacturerService{
    fun getManufacturers(): List<Manufacturer>
    fun save(manu: ManufacturerDto):Manufacturer
        

}