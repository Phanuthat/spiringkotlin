package dv.spring592110131.demo.service

import dv.spring592110131.demo.entity.Product
import org.springframework.data.domain.Page

interface ProductService {
    fun getProduct() : List<Product>
    fun getProductByname(name:String):Product?
    fun getProductByPartialName(name: String): List<Product>
    fun getProductByPartialNameDes(name: String, desc: String): List<Product>
    fun getProductByManuName(name: String): List<Product>
    fun getProductWithPage(name: String, page: Int, pageSize: Int): Page<Product>
    fun save(manuId:Long,product: Product):Product
    fun remove(id: Long): Product?


}